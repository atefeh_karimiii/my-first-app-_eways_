package com.example.eways_1.repository.network;

import com.example.eways_1.model.LoginRequest;
import com.example.eways_1.model.LoginResponse;
import com.example.eways_1.model.Product;
import com.example.eways_1.model.ProductResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIInterface {



    @POST("user/login")
    Call<LoginResponse> loginUser(@Body LoginRequest loginRequest);

    @POST("store/getfilteredproducts")
    Call<ProductResponse> getProducts(@Body Product product);

}
