package com.example.eways_1.repository.local;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;


import com.example.eways_1.Constant;

import static android.content.Context.MODE_PRIVATE;
import static com.example.eways_1.Constant.APP_KEY;

public class EwaysSharedPreference {

    private static EwaysSharedPreference mInstance;

    private SharedPreferences sharedPreferences;


    public static EwaysSharedPreference getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new EwaysSharedPreference(context);
        }
        return mInstance;
    }

    public EwaysSharedPreference(Context mContext) {
        sharedPreferences = mContext.getSharedPreferences(APP_KEY, MODE_PRIVATE);
    }

    public String getToken() {
        return sharedPreferences.getString("TOKEN_KEY", "");
    }

    public void putToken(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("TOKEN_KEY", token).apply();
    }


}