package com.example.eways_1.repository.network;

import android.content.Context;

import com.example.eways_1.Constant;
import com.example.eways_1.repository.local.EwaysSharedPreference;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Objects;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.eways_1.Constant.APP_KEY;

public class HeaderInterceptor implements Interceptor {

    private Context mContext;
    private Constant mConstant;

    private static HeaderInterceptor mInstance;
    private HeaderInterceptor(){}
    Context context;


    public static HeaderInterceptor getInstance(){

        if (mInstance == null){

            mInstance = new HeaderInterceptor();
        }

        return mInstance;
    }


    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String token = EwaysSharedPreference.getInstance(context).getToken();

        Request.Builder requestBuilder = original.newBuilder()
                .header("appkey",APP_KEY);

        if (!token.isEmpty()){
                requestBuilder.addHeader("Authorization", token);
        }

        Request request = requestBuilder.build();
        return chain.proceed(request);

    }
}