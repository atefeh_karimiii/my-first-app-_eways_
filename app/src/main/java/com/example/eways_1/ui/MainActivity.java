package com.example.eways_1.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.example.eways_1.R;
import com.example.eways_1.model.LoginRequest;
import com.example.eways_1.model.LoginResponse;
import com.example.eways_1.model.Product;
import com.example.eways_1.model.ProductResponse;
import com.example.eways_1.repository.local.EwaysSharedPreference;
import com.example.eways_1.repository.network.APIClient;
import com.example.eways_1.repository.network.APIInterface;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login();
    }

    private void login() {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        LoginRequest loginRequest = new LoginRequest("at.karimi", "12345678", "null", 8, "JXZYtqDmdPqpHkYL", true);
        Call<LoginResponse> call = apiInterface.loginUser(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
//                    EwaysSharedPreference sharedPreference = new EwaysSharedPreference(getBaseContext());
                    EwaysSharedPreference.getInstance(getBaseContext()).putToken(response.body().getToken());

                    productsRequest();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }

        });
    }

    private void productsRequest() {

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Product product = new Product(0,"","",0,0,"",0,true,0,0,0,0,0,0,true,0,"");
        Call<ProductResponse> call = apiInterface.getProducts(product);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()) {
                    List<Product> products = response.body().getProducts();
                    setupRecyclerView(products);
                }

            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    Toast.makeText(MainActivity.this, "Connection Problem !", Toast.LENGTH_SHORT).show();

                }

            }
        });


    }

    private void setupRecyclerView(List<Product> products) {

        RecyclerView recyclerView;
        Adapter adapter;

        recyclerView = findViewById(R.id.recycler_view);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        adapter = new Adapter(this, products);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

}