package com.example.eways_1.model;

import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    private int id ;

    @SerializedName("name")
    private String name;

    @SerializedName("seoName")
    private String seoName;

    @SerializedName("price")
    private int price;

    @SerializedName("oldPrice")
     private int oldPrice;

    @SerializedName("imageUrl")
    private String imageUrl;

    @SerializedName("stoke")
    private int stock;

    @SerializedName("availability")
    private boolean availability;

    @SerializedName("minOrder")
    private int minOrder;

    @SerializedName("maxOrder")
    private int maxOrder;

    @SerializedName("overInventoryCount")
     private int overInventoryCount;

    @SerializedName("point")
    private int point;

    @SerializedName("discount")
    private int discount;

    @SerializedName("lawId")
    private int lawId;

    @SerializedName("isSim")
    private boolean isSim;

    @SerializedName("brandId")
    private int brandId;

    @SerializedName("brandName")
    private String brandName;

    public Product(int id, String name, String seoName, int price, int oldPrice, String imageUrl, int stock, boolean availability, int minOrder, int maxOrder, int overInventoryCount, int point, int discount, int lawId, boolean isSim, int brandId, String brandName) {
        this.id = id;
        this.name = name;
        this.seoName = seoName;
        this.price = price;
        this.oldPrice = oldPrice;
        this.imageUrl = imageUrl;
        this.stock = stock;
        this.availability = availability;
        this.minOrder = minOrder;
        this.maxOrder = maxOrder;
        this.overInventoryCount = overInventoryCount;
        this.point = point;
        this.discount = discount;
        this.lawId = lawId;
        this.isSim = isSim;
        this.brandId = brandId;
        this.brandName = brandName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeoName() {
        return seoName;
    }

    public void setSeoName(String seoName) {
        this.seoName = seoName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(int oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public int getMinOrder() {
        return minOrder;
    }

    public void setMinOrder(int minOrder) {
        this.minOrder = minOrder;
    }

    public int getMaxOrder() {
        return maxOrder;
    }

    public void setMaxOrder(int maxOrder) {
        this.maxOrder = maxOrder;
    }

    public int getOverInventoryCount() {
        return overInventoryCount;
    }

    public void setOverInventoryCount(int overInventoryCount) {
        this.overInventoryCount = overInventoryCount;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getLawId() {
        return lawId;
    }

    public void setLawId(int lawId) {
        this.lawId = lawId;
    }

    public boolean isSim() {
        return isSim;
    }

    public void setSim(boolean sim) {
        isSim = sim;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
