package com.example.eways_1.model;

import com.google.gson.annotations.SerializedName;

public class LoginRequest {

    @SerializedName("userName")
    private String userName;

    @SerializedName("password")
    private String password;

    @SerializedName("info")
    private String info;

    @SerializedName("type")
    private int type;

    @SerializedName("appKey")
    private String appKey;

    @SerializedName("rememberMe")
    private boolean rememberMe;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }


    public LoginRequest(String userName, String password, String info, int type, String appKey, boolean rememberMe) {
        this.userName = userName;
        this.password = password;
        this.info = info;
        this.type = type;
        this.appKey = appKey;
        this.rememberMe = rememberMe;
    }
}
